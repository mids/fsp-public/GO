# Geschäftsordnungs Repo

Die eigentliche [Geschäftsordnung des Fachschaftsparlamentes Mathematik und Informatik findest du hier](Go.md).

## (relevante) Übergeordnete Rechtstexte, Gesetze und Ordnungen

Keine Gewähr auf Vollständigkeit!

- NHG
- OrgS
- FinO
- Geschäftsordnung StuPa
- Niedersächsische Verfassung
- Hochschulrahmengesetz
- Niedersächsisches Justizgesetz
- Niedersächsisches Verwaltungsverfahrengesetz
- Verwaltungsverfahrengesetz
- Grundgesetz
