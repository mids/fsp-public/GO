# Geschäftsordnung des Fachschaftsparlaments der Fachschaft Mathematik und Informatik

### §1 Konstituierung

1. In der ersten Sitzung obliegt dem Präsidiumsvorsitz des vorhergehenden Parlaments bis zur Wahl eines Präsidiumsvorsitz die Sitzungsleitung. Ist dieser nicht erschienen, oder ist diesem die Sitzungsleitung nicht möglich, so übernimmt das älteste Mitglied die Pflichten nach Satz 1. Lehnt das Mitglied dies aufgrund wichtiger Gründe ab, so übernimmt das jeweils nächstälteste Mitglied diese Pflichten.
2. Der nach Absatz 1 bestimmte Präsidiumsvorsitz (Alterspräsidiumsvorsitz) ernennt aus den Parlamentsmitgliedern die vorläufige Schriftführung.
3. Nach Feststellung der Beschlussfähigkeit wird die Wahl des Präsidiumsvorsitz, der Stellvertretung und der Schriftführung vorgenommen.

## I. Präsidium

### §2 Sitzungsleitung

1. Das Präsidium, bestehend aus dem Präsidiumsvorsitz und etwaigen Stellvertretungen sowie der Schriftführung, leitet die Verhandlung.
2. Dem Präsidium obliegt, unbeschadet etwaiger Anträge zur Geschäftsordnung, die Auslegung der Satzung und der Ergänzungsordnungen für alle die Verhandlung betreffenden Fragen. Dabei entscheidet das Präsidium durch Beratung. Die Stimme des Präsidiumsvorsitz ist ausschlaggebend.
3. Der Präsidiumsvorsitz darf sich nur in Angelegenheiten der Geschäftsordnung an der Diskussion beteiligen. Um sich in anderen Angelegenheiten zur Sache äußern zu können, muss dieser sich vorübergehend vertreten lassen. Hat der Präsidiumsvorsitz sich einmal zu einem Punkt der Tagesordnung geäußert, kann er bis zum Ende der Beratung das Amt des Präsidiumsvorsitz nicht mehr übernehmen.
4. Muss der Präsidiumsvorsitz vertreten werden, so übernehmen dies seine Stellvertretungen in ihrer bei der Wahl bestimmten Reihenfolge. 

### §3 Konstruktives Misstrauensvotum

1. Das Parlament kann dem Präsidiumsvorsitz oder einer Stellvertretung das Misstrauen dadurch aussprechen, dass es mit der Mehrheit seiner Mitglieder eine Nachfolge wählt. Bei einem derartigen Misstrauensantrag leitet die Stellvertretung, in Ermangelung eines solchen die Schriftführung, bei Uneinigkeit der Schriftführung entscheidet ein fairer Würfelwurf.

### §4 Einladung

1. Die Einladung zur Sitzung des Parlamentes ist zusammen mit dem Protokoll der letzten Sitzung spätestens eine Woche vor der Sitzung vom Präsidiumsvorsitzen an alle Mitglieder zu verschicken.
2. Die Einladung enthält die Ankündigung von Personalwahlen und die vorläufige Tagesordnung.
3. Im Falle der Haushalts- und der Nachtragshaushaltsberatung soll den Einladungen ein entsprechender Entwurf beiliegen.

### §5 Sitzungseröffnung, Beschlussfähigkeit, ordnungsgemäße Ladung, Feststellung der Tagesordnung

1. Zu Beginn jeder Sitzung stellt der Präsidiumsvorsitz die Anwesenheit sowie die Tatsache der Beschlussfähigkeit fest. Dann wird die Schriftführung benannt.
2. Das Parlament ist beschlussfähig, wenn die Sitzung ordnungsgemäß einberufen wurde und die Mehrheit der stimmberechtigten Mitglieder anwesend ist.
3. Die Beschlussfähigkeit bleibt bestehen, bis der Präsidiumsvorsitz die Beschlussunfähigkeit feststellt. Auf Antrag muss die Beschlussfähigkeit festgestellt werden; das antragsstellende Mitglied zählt zu den Anwesenden. Ist die Sitzung bis spätestens dreißig Minuten nach festgesetzter Uhrzeit nicht eröffnet worden, so gilt das Parlament als beschlussunfähig.
4. Nach der Feststellung der Anwesenheit und der Beschlussfähigkeit und der Benennung der Schriftführung beschließt das Parlament, unter Berücksichtigung der gestellten Änderungs- und Ergänzungsanträge, über die endgültige Tagesordnung.

### §6 Anwesenheit

1. Das Präsidium kann die Anwesenheit feststellen durch:
    1. namentlichen Aufruf der Mitglieder oder
    2. auslegen einer Anwesenheitsliste, in die sich die Mitglieder eintragen.
2. Im Falle eines frühzeitigen Verlassens der Sitzung muss sich das Mitglied beim Präsidium abmelden.
3. Muss ein Mitglied begründet von der Sitzung fernbleiben, ist der Präsidiumsvorsitz unverzüglich zu informieren.
4. Anwesend ist, wer sich im Parlamentsraum befindet und namentlich in der Anwesenheitsliste steht.

## II. Tagesordnung

### §7 Aufstellung

1. Die vorläufige Tagesordnung wird vom Präsidiumsvorsitzen im Benehmen mit den Stellvertretungn aufgestellt. Hierzu sind Anträge zur Änderung und Ergänzung möglichst frühzeitig beim Präsidium einzureichen. Sie können spätestens bis zur Beschlussfassung über die Tagesordnung gestellt werden.

### §8 Inhalt

1. Die Tagesordnung muss mindestens enthalten:
    1. Feststellung der ordnungsgemäßen Ladung und der Beschlussfähigkeit;
    2. Bennenung der Schriftführung;
    3. Beschlussfassung über die endgültige Tagesordnung;
    4. Genehmigung des Protokolls der letzten Sitzung;
    5. Bericht aus dem FSR;
    6. Fragen an den FSR;
    7. Verschiedenes.
2. Soweit Anlass dazu gegeben ist, sind auf die Tagesordnung aufzunehmen:
    1. Bekanntmachungen;
    2. Beratungsanordnungen;
    3. Anfragen.
3. Die Neuwahl eines Präsidiumsvorsitz folgt unmittelbar der Benennung der Schriftführung. Im Übrigen gehen die in Absatz 1 genannten Tagesordnungspunkte anderen Tagesordnungspunkten stets voraus. Ausgenommen ist Absatz 1 Nummer 7, der stets letzter Tagesordnungspunkt ist. Inhaltliche Anträge sollten Wahlen möglichst vorausgehen.

### §9 Antrags- und Anfragerecht

1. Jedes Fachschaftsmitglied hat beim Parlament ein Antrags- und Anfragerecht.

### §10 Aufnahme und Entfernung von Anträgen

1. Das Präsidium hat schriftliche Anträge, die ihrem Wesen nach nicht vor der Aufstellung der Tagesordnung eingereicht werden konnten (Dringlichkeitsanträge), zusätzlich auf die Tagesordnung aufzunehmen.
2. Das Parlament kann durch Beschluss schriftliche Anträge, die keine Dringlichkeitsanträge im Sinne des Absatzes 1 sind, auf die Tagesordnung aufnehmen. Der Beschluss hat ohne vorausgegangene Begründung und Beratung des Antrages zu erfolgen.
3. Die Entfernung eines Antrages von der Tagesordnung kann nicht beschlossen werden, falls mindestens 2 anwesende Mitglieder widersprechen.

### §11 Anfragen außerhalb der aufgestellten Tagesordnung

1. Zu Beginn jeder ordentlichen Sitzung können von Mitgliedern der Fachschaft kurze Anfragen gestellt werden, deren Wortlaut dem Präsidiumsvorsitz vor Beginn der Sitzung vorliegen muss. Der Präsidiumsvorsitz verliest die vom Präsidium zugelassenen Anfragen.

### §12 Fraktionspausen
1. Das Präsidium genehmigt auf Antrag Fraktionspausen in angemessener und zumutbarer Länge. Diese Genehmigung kann mit der absoluten Mehrheit rückgängig gemacht werden. Genehmigt das Präsidium eine beantragte Fraktionspause nicht, so kann sie das Parlament mit der absoluten Mehrheit genehmigen. Nach Ablauf der Fraktionspause tritt das Parlament wieder zusammen. Ist nicht innerhalb von fünfzehn Minuten nach Ablauf der genehmigten Fraktionspause die Sitzung wieder eröffnet, so gilt das Parlament als beschlussunfähig.

## III. Verhandlungsordnung

### §13 Aufnahme der Antragsberatung

1. Der Präsidiumsvorsitz stellt Haupt- und Nebenanträge durch ausdrückliche Erklärung zur Beratung. Danach wird über sie nach Maßgabe der Vorschriften dieses Abschnittes beraten.

### §14 Haushaltsberatung

1. Bei der Beratung des Haushaltsplanes oder bei der Beratung von Nachtragshaushalten können Anträge, die zu einer Mehrausgabe oder Mindereinnahme gegenüber dem Haushaltsplan oder den Nachtragshaushalten führen, nur dann gestellt werden, wenn die Anträge gleichzeitig Vorschläge zur Deckung der durch sie entstehenden Mehrausgaben oder Mindereinnahmen enthalten. Antrag und Ausgleichsvorschlag bilden für die Beratung einen einheitlichen und unteilbaren Antrag.
2. Der Entwurf eines Haushaltsplanes kann im Parlament nicht beraten werden, wenn nicht das Finanzreferat des FSR Gelegenheit zur Beratung und Abgabe einer Empfehlung hatte.

### §15 Finanziell belastende Vorlagen und Anträge

1. Werden Vorlagen und Anträge im Parlament eingebracht, die geeignet sind, die Finanzgebarung der Studierendenschaft in der Gegenwart oder Zukunft zu belasten, und führen diese Mehrausgaben nach Auskunft des Finanzreferates zu einer Überschreitung des Haushaltsplanes, oder reichen nach Auskunft des Finanzreferates die im Haushaltsplan vorgesehenen Mittel nicht zur Deckung aus, so kann der Antrag nur beraten werden, wenn zuvor die erforderlichen Mittel im Rahmen eines Nachtragshaushalts zur Verfügung gestellt worden sind.

### §16 1. Lesung

1. In der Eingangsberatung (1. Lesung) begründen die antragsstellenden Personen ihren Antrag. Das Parlament kann beschließen den Antrag an einen Arbeitsausschuss zu überweisen, nicht in die Einzelberatung einzutreten oder diese zu vertagen.

### §17 2. Lesung

1. In der Einzelberatung (2. Lesung) stellt der Präsidiumsvorsitz den Antrag abschnittsweise zur Beratung. Es werden Abänderungs- und Zusatzanträge gestellt. Diese müssen schriftlich beim Präsidium eingereicht werden.

### §18 3. Lesung

1. In der Schlussberatung (3. Lesung) verliest der Präsidiumsvorsitz den abstimmungsreifen Antrag. Wenn zu diesem als ganzem keine Wortmeldung mehr vorliegen, erhalten die antragsstellenden Personen das Schlusswort.
2. In der Schlussberatung können Anträge auf abschnittsweise Beschlussfassung, Vertagung oder Überweisung an einen Ausschuss gestellt werden.

### §19 3. Lesung des Haushaltsplanes

1. In die Schlussberatung (3. Lesung) eines Haushaltsplanes kann nicht eingetreten werden, wenn dem FSR nicht vorher Gelegenheit gegeben wurde den Haushaltsplan zu beraten und zu bestätigen.

### §20 Antrag auf Nichtbefassung

1. Ein Antrag auf Nichtbefassung ist nur vor dem Eintritt in die Debatte über den zur Beratung stehenden Antrag zulässig.
2. Findet die Beratung über einen Nichtbefassungsantrag statt, so steht den ursprünglichen antragsstellenden Personen vorher ein Schlusswort zu.

## IV. Redeordnung

### §21 Worterteilung

1. Der Präsidiumsvorsitz erteilt das Wort in der Reihenfolge der Wortmeldungen. Für- und Gegenrede sollen nach Möglichkeit abwechselnd zu Wort kommen.

### §22 Beschränkung der Redezeit

1. Ein Antrag auf Beschränkung der Redezeit kann jederzeit gestellt werden. Die Beschränkung gilt bis zur nächsten Abstimmung. Das Schlusswort kann nicht beschränkt werden.

### §23 Schließung der Redeliste

1. Im Verlaufe der Beratung über einen Gegenstand kann durch Beschluss des Parlamentes die Redeliste geschlossen werden.

### §24 Antrag auf Schluss der Debatte

1. Ein Antrag auf Schluss der Debatte kann nur gestellt werden, wenn mindestens je zwei Für- und Gegenrededen des zur Debatte stehenden Antrages gehalten werden konnten.
2. Wird ein Antrag auf Schluss der Debatte gestellt, so ist nach Anhören einer Gegenrede sofort darüber abzustimmen. Eine begründete Gegenrede geht einer formalen Gegenrede vor. Wird der Antrag angenommen, so steht nur noch den antragsstellenden Personen des zur Beratung stehenden Antrages das Schlusswort zu.

### §25 Antrag auf sofortige Abstimmung

1. Ein Antrag auf sofortige Abstimmung kann nur gestellt werden, wenn bereits ein Antrag nach §22, §23 oder §24 gestellt wurde oder mindestens 5 Anträge zur Geschäftsordnung nacheinander gestellt wurden.
2. Wird ein Antrag auf sofortige Abstimmung gestellt, so ist sofort darüber abzustimmen. Eine Annahme des Antrags Bedarf einer Zweidrittelmehrheit. Wird der Antrag angenommen, so ist sofort über alle vorliegenden Anträge und Anträge zur Geschäftsordnung, nach Maßgaben des §36 nacheinander abzustimmen. Dabei sind nur noch der Antrag auf namentliche oder der Antrag auf geheime Wahl stellbar, Wortmeldungen sind bis zum nächsten Tagesordnungspunkt ausgeschlossen.

### §26 Anträge und Ausführungen zum Verfahren und zur Geschäftsordnung

1. Personen, die zum Verfahren Ausführungen machen oder Anträge zur Geschäftsordnung stellen wollen, erhalten das Wort außerhalb der Redeliste. Eine Person, die das Wort zur Geschäftsordnung erhalten hat, darf sich nur zur verfahrensmäßigen Behandlung des gerade anstehenden Tagesordnungspunktes oder zum gestellten Geschäftsordnungsantrag äußern.
2. Geschäftsordnungsanträge sind zur Abstimmung zu stellen, bevor die Beratung zur Sache im Hinblick auf den gerade anstehenden Tagesordnungspunkt fortgesetzt wird.

### §27 Persönliche Erklärung

1. Nach dem Abschluss der Behandlung eines Tagesordnungspunktes können Personen das Wort zu einer persönlichen Erklärung erhalten. Dabei dürfen sie nicht zur Sache sprechen, sondern nur Äußerungen, die in der Aussprache in Bezug auf ihre Person vorgekommen sind, zurückweisen oder eigene Ausführungen richtigstellen. Sie dürfen nicht länger als drei Minuten sprechen.

### §28 Beratungsteilnahme von Nichtmitgliedern

1. Nichtmitglieder sind nach Beschluss des Parlamentes berechtigt, an der Beratung teilzunehmen.
2. FSR-Mitglieder, FSR-Angehörige und Fachgruppenvertretungen haben ein Rede- und Anhörungsrecht.

### §29 Ordnungsrecht des Präsidiumsvorsitzes

1. Der Präsidiumsvorsitz kann zur Ordnung und zur Sache rufen und nach zweimaliger Verwarnung das Wort entziehen, solange über den fraglichen Punkt verhandelt wird. Bei ungebührlichem Benehmen eines Mitgliedes oder einer zuschauenden Person ist er berechtigt, die Person des Raumes zu verweisen.
2. Das Parlament kann eine solche Maßnahme rückgängig machen. Hierbei ist die betroffene Person weder antrags- noch stimmberechtigt.

## V. Abstimmung

### §30 Zeitpunkt der Abstimmung

1. Nach Abschluss der Verhandlung über alle Haupt- und Nebenanträge ist nach Maßgabe der Vorschriften dieses Abschnittes abzustimmen.

### §31 Beschlussfassung

1. Das Parlament fasst, soweit satzungsmäßig nicht anders bestimmt, seine Beschlüsse mit einfacher Stimmenmehrheit bei Anwesenheit der Mehrheit seiner Mitglieder.

### §32 und §33

*gestrichen*

### §34 Schriftliches Beschlussverfahren

1. Bei dem schriftlichen Beschlussverfahren können die Abstimmenden innerhalb einer Woche auf schriftlichem Wege ihre Stimme abgeben.
2. Geheime Wahlen im Schriftlichen Beschlussverfahren sind unzulässig. Beantragt ein Mitglied eine geheime Wahl, so ist eine etwaige bereits im Gang befindliche Wahl im Umlaufverfahren abzubrechen, eine Sitzung innerhalb von 21 Tagen anzuberaumen und ein entsprechender Tagesordnungspunkt, bei dem die Wahl wiederholt wird, auf die Tagesordnung zu setzen.

### §35 Personalwahlen

1. Auf Personalwahlen finden die Bestimmungen der Satzung dieser Geschäftsordnung über Beschlüsse Anwendung.
2. Erklärt eine gewählte Person ihren Rücktritt, so ist frühestens nach Ablauf von sieben Tagen eine Sitzung zur Neuwahl einzuberaumen. Dies gilt nicht, wenn der Rücktritt gelegentlich einer Neuwahl erklärt wird, zu der ordnungsgemäß geladen wurde, oder wenn mit der Rücktrittserklärung einem gleichzeitig anstehenden konstruktiven Misstrauensvotum vorgegriffen werden soll.

### §36 Geheime und namentliche Abstimmung

1. Auf Antrag von Mitgliedern erfolgt die Abstimmung geheim oder namentlich. Einer Abstimmung über einen solchen Antrag bedarf es nicht. Der Antrag auf geheime Abstimmung hat Vorrang.
2. Bei namentlicher Abstimmung sind die Namen der mit "Ja" und mit "Nein" stimmenden, sowie der sich der Stimme enthaltenden Mitglieder in das Protokoll aufzunehmen.

### §37 Mehrheit von Anträgen

1. Von mehreren zu einem Beratungspunkt vorliegenden Anträgen sind die unterschiedlich weitgehenden Anträge nacheinander beginnend mit dem weitestgehenden abzustimmen. Im Übrigen sind Anträge alternativ abzustimmen.

### §38 Entlastung

1. Das Parlament kann eine Entlastung nicht beschließen, wenn nicht vorher eine Stellungnahme des FSR gehört wurde.

## VI. Beschlussprotokoll

### §39 Anfertigung, Inhalt und Veröffentlichung des Beschlussprotokolls

1. Über die Verhandlung des Parlaments wird von der Schriftführung binnen dreier Tage nach jeder Sitzung ein Beschlussprotokoll angefertigt, das von ihnen und von dem Präsidiumsvorsitz des Parlamentes zu unterzeichnen ist.
2. Dieses Beschlussprotokoll enthält:
    1. eine Zusammenfassung der Berichte und der Beantwortung der gestellten Fragen;
    2. die Namen der Kandidaten für Personalwahlen, die Namen der Vorschlagenden und das Ergebnis der Wahl;
    3. den Wortlaut aller Haupt- und Nebenanträge, die Namen aller antragsstellenden Personen und das Ergebnis der Abstimmung;
    4. Erklärung zu Protokoll, die dem Präsidiumsvorsitz schriftlich eingereicht werden müssen und eine halbe Seite (8 pt.) nicht überschreiten dürfen.
3. Das Beschlussprotokoll ist nach seiner Genehmigung durch das Parlament zu veröffentlichen.

## VII. Sonstige Bestimmungen

### §40 Ausschüsse

1. Über den Vorsitz und die Verhandlungsweise der Ausschüsse und Kommissionen entscheiden diese selbst. Sie verhandeln in öffentlicher Sitzung, sofern nicht anders beschlossen.
2. In allen Ausschüssen des Parlamentes, die sich mit Hochschul-, Studien- und Haushaltsfragen befassen, nimmt ein von dem FSR gewähltes Mitglied mit beratender Stimme teil.

### §41 Schriftlichkeit
1. Sofern es sich nur um Belange des Parlamentes handelt erfüllen auch folgende Kommunikationsformen die Schriftform:
    1. eine E-Mail welche mit einer digitalen Signatur unterzeichnet wurde, die genau einem Parlamentsmitglied zugeordnet und dem Präsidium bekannt ist
    2. In Ausnahmefällen, eine E-Mail von der dem Mitglied zugehörigen, dem Präsidium bekannten, studentischen E-Mail Adresse
    3. ein Brief
    4. die vom FSR genutzte Kommunikationsplattform

### §42 Änderung der Geschäftsordnung

1. Die Geschäftsordnung kann nur mit qualifizierter Zweidrittelmehrheit der satzungsmäßigen Mitglieder des Parlamentes geändert werden.
2. Diese Geschäftsordnung kann mit qualifizierter Mehrheit in folgenden Maße, für die Dauer einer Legislaturperiode, vereinfacht werden:
    1. §§ 2 Abs. 3, 6, 10 Abs. 2, 14 Abs. 2, 16, 17, 18, 19 entfallen
    2. Alle nach dieser Geschäftsordnung als verpflichtend schriftlich einzureichenden Anträge können auch mündlich eingereicht werden.
    3. Sonderparagraphen S§1 und S§2 treten in Kraft.

### §43 Abweichungen von dieser Geschäftsordnung

1. Abweichungen von den Vorschriften dieser Geschäftsordnung können im einzelnen Fall mit Zweidrittelmehrheit der anwesenden Mitglieder des Parlamentes beschlossen werden, wenn die Bestimmungen übergeordneter Gesetze dem nicht entgegenstehen.

### §44 Inkrafttreten

1. Diese Geschäftsordnung tritt durch Verabschiedung ihrer Beschlussfassung in Kraft.

# Anlage 1 Sonderparagraphen

### S§1 Lesung
1. In der Beratung begründen die antragsstellenden Personen ihren Antrag. Es werden Abänderungs- und Zusatzanträge gestellt. Nach diesen Abstimmungen erhalten die antragsstellenden Personen das Schlusswort.
2. In der Beratung können Anträge auf abschnittsweise Beschlussfassung, Vertagung oder Überweisung an einen Ausschuss gestellt werden.

### S§2 Abstimmungen per Umlauf
1. Sofern nicht durch eine übergeordnete Ordnung ausgeschlossen, können alle Beschlüsse ohne Zusammenkunft durch Gegenzeichnen der Beteiligten auf schriftlichem Wege gefasst werden (Schriftliches Beschlussverfahren).
2. Bei diesem Verfahren müssen mindestens Zweidrittel der Abstimmungsberechtigten aktiv mit "Ja" abstimmen, damit Anträge, die auf diese Art abgestimmt werden, angenommen werden.
3. In dringlichen Fällen ist eine einfache Mehrheit ausreichend.
4. Das Parlament kann einstimmig beschließen, dass die Wahlhandlung mit Erreichen der erforderlichen "Ja" oder "Nein" Anzahl beendet ist.
